### Description of the Asset : ###

* This asset is used to post chatter notification with mentions and attachments(if any)
* Developed for use in Apex only.
* Can easily be modified to suit one's needs according to a use case.

### Purpose of the Asset : ###

* This asset is an implementation of post chatter notification using ConnectApi.
* This asset is a very basic example of how we can post chatter notification with mentions and attachments and can be refined further to support any use case irrespective of its complexity.
* See screenshots for how the component looks.

### In this asset ###

* An apex class which has a utility method postChatterNotification() 
* postChatterNotification() can be called by any method to post to chatter.
* postChatterNotification has following paramenters - 
    * userId(optional) - user Id whom you want to Mention
    * notificationTemplate(optional) - It contains text of Post
    * parentId(mandatory) - The ID of the parent this feed element is being posted to.
    * fileIds(optional) - Content Document Id list of files you want to attach.

### How it all works? : ###

* The ConnectApi namespace (also called Connect in Apex) provides classes to Get, post, and delete feed elements, likes, comments, and bookmarks. You can also search feed elements, share feed elements, and vote on polls.
* Following are the classes used in this asset and their use - 
    * ConnectApi.​​FeedItem​​Input - Used to create rich feed items, for example, feed items that include @mentions or files.
    * ConnectApi.MentionSegmentInput - Include an @mention of a user or group in a feed post or comment. When creating a feed post or comment, you can include up to 25 mentions.
    * ConnectApi.​Message​​Body​Input - Used to add rich messages to feed items and comments.
    * ConnectApi.TextSegmentInput - Used to include a text segment in a feed item or comment.
    * ConnectApi.​FilesCapabilityInput - Attach up to 10 files that have already been uploaded or remove one or more files from a feed element.
    * ConnectApi.​FeedElement​CapabilitiesInput - A container for all capabilities that can be included when creating a feed element. (I have just used files property in this assets).
* ConnectApi.FeedItemInput is the parent which contains all other things - 
    * body - body of post which contains text,files, mentions etc.
    * feedElementType - The type of feed element this input represents. Required when creating a feed element. Optional when updating a feed element.
    * subjectId - The ID of the parent this feed element is being posted to. This value can be the ID of a user, group, or record, or the string me to indicate the context user.
    * capabilities - The capabilities that define additional information on this feed element.
* body of ConnectApi.FeedItemInput contains ConnectApi.MessageBodyInput which further contains following - 
    * It contains multiple messageSegments and each messageSegment can contain ConnectApi.MentionSegmentInput(mentions) or ConnectApi.TextSegmentInput(plain text of the field).

### Steps to utilize this asset : ###

* postChatterNotification() is a utility function that can be called by any class. Please use the below code to call the method - 
    PostChatterNotificationUtility.postChatterNotification(userIdToMention,textOfPost,parentId,listOfContentDocIds);

### Who do I talk to? ###

* Mahima Aggarwal